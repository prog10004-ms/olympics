from athlete import Athlete

#STEP 1: open the file
with open('./data/olympics.csv', 'r') as olympicsDataFile:
    #keep track of the athletes registered in the competition
    athleteRegistry = []

    #STEP 2: iterate through the file content
    headerRead = False
    for athleteRec in olympicsDataFile:   
        #skip the header row of the file
        if not headerRead:
            headerRead = True
            continue #skip the rest of the loop

        #STEP 2a: read the athlete record, 
        #The record format is: Name,Gender,Age,Team,Event,Medal
        athleteData = athleteRec[:-1].split(',')

        #STEP 2b: use the data read from teh file
        #create an athelete object with the data read from the file
        athlete = Athlete(athleteData[0], athleteData[3])
        athlete.setAge(int(athleteData[2]))
        athlete.setGender(athleteData[1])
        athlete.setEvent(athleteData[4])
        athlete.setMedal(athleteData[5])

        #print the athlete information
        print(athlete)

        #register the athlete
        athleteRegistry.append(athlete)

        #STEP 3: closing the file is not longer needed because the context manager will close the file
        #even when exceptions occurr

#create a file with the medalists
with open('./data/medalists.csv', 'w') as medalistDataFile:
    #iterate through all athletes and save the ones that won a medal
    for athlete in athleteRegistry:
        #check if athlete won a medal
        if athlete.wonMedal():
            #write the athlete record to the file
            medRecord = f'{athlete.getName()}, {athlete.getTeam()}, {athlete.getEvent()}, {athlete.getMedal()}\n'
            medalistDataFile.write(medRecord)