"""Define the Athlete class whose instances are saved (serialized) as records into the olympics.csv file"""
class Athlete:
    def __init__(self, name, team):
        self._name = name
        self._gender = ''
        self._age = 0
        self._team = team
        self._event = ''
        self._medal = ''

    def getName(self):
        return self._name
    
    def setName(self, name):
        self._name = name

    def getGender(self):
        return self._gender

    def setGender(self, gender):
        self._gender = gender
        
    def getAge(self):
        return self._age

    def setAge(self, age):
        self._age = age

    def getTeam(self):
        return self._team

    def setTeam(self, team):
        self._team = team

    def getEvent(self):
        return self._event
    
    def setEvent(self, event):
        self._event = event

    def getMedal(self):
        return self._medal

    def setMedal(self, medal):
        self._medal = medal

    def wonMedal(self):
        return self._medal != 'NA'

    def __str__(self):
        return f'{self._name} is part of team {self._team} in event {self._event}'
    
